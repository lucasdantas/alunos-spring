package br.ufrn.imd.ppgsw.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ufrn.imd.ppgsw.domain.Aluno;

public interface AlunoRepository extends JpaRepository<Aluno, Long> {

}
