package br.ufrn.imd.ppgsw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlunosSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlunosSpringApplication.class, args);
	}
}
