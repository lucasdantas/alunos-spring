package br.ufrn.imd.ppgsw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.ufrn.imd.ppgsw.domain.Aluno;
import br.ufrn.imd.ppgsw.repository.AlunoRepository;

@Controller
@RequestMapping("/alunos")
public class AlunosController {

	@Autowired
	private AlunoRepository alunoRepository;

	@GetMapping
	public ModelAndView listar() {
		ModelAndView modelAndView = new ModelAndView("ListaAlunos");
		modelAndView.addObject("alunos", alunoRepository.findAll());
		modelAndView.addObject(new Aluno());
		return modelAndView;
	}

	@PostMapping
	public String salvar(Aluno aluno) {
		alunoRepository.save(aluno);			
		return "redirect:/alunos";
	}

}
